#!/usr/bin/env php
<?php
// application.php

require __DIR__.'/vendor/autoload.php';
use Symfony\Component\Console\Application;
use App\Command\SendPassCommand;
use Illuminate\Database\Capsule\Manager as Capsule;


$dotenv = Dotenv\Dotenv::create(__DIR__); // para las varibles conexion 'DB_HOST', ETC
$dotenv->load();

$container = new DI\Container(); //implementando contenedor de inyeccions de dependencias con php-di/php-di

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => getenv('BD_DRIVER'),
    'host'      => getenv('BD_HOST'),
    'database'  => getenv('BD_NAME'),
    'username'  => getenv('BD_USER'),
    'password'  => getenv('BD_PASS'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal(); //hace todo como si estuvierae en el contexto global

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent(); //inicializa eloquent


$application = new Application();
$application->add(new SendPassCommand());
$application->run();