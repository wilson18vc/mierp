<?php

ini_set("display_errors",1);
ini_set('display_starup_error',1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php';
session_start();

$dotenv = Dotenv\Dotenv::create(__DIR__.'/..'); // para las varibles conexion 'DB_HOST', ETC
$dotenv->load();

use App\Middlewares\AuthenticationMiddleware;
use Middlewares\AuraRouter;
use WoohooLabs\Harmony\Harmony;
use WoohooLabs\Harmony\Middleware\DispatcherMiddleware;
use WoohooLabs\Harmony\Middleware\HttpHandlerRunnerMiddleware;
use Zend\Diactoros\Response;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter; //middleware q emite respuestas
use Aura\Router\RouterContainer;
use Illuminate\Database\Capsule\Manager as Capsule;
use App\Models\Costos;
use Zend\Diactoros\Response\HtmlResponse;
use Monolog\Logger; //dependencia para crear nuestros archivos logs
use Monolog\Handler\StreamHandler; //para indicarle donde estaran nuestros logs
use Franzl\Middleware\Whoops\WhoopsMiddleware;

$log = new Logger('app');
$log->pushHandler(new StreamHandler(__DIR__.'/../logs/app.log', Logger::WARNING));


$container = new DI\Container(); //implementando contenedor de inyeccions de dependencias con php-di/php-di

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => getenv('BD_DRIVER'),
    'host'      => getenv('BD_HOST'),
    'database'  => getenv('BD_NAME'),
    'username'  => getenv('BD_USER'),
    'password'  => getenv('BD_PASS'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal(); //hace todo como si estuvierae en el contexto global

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent(); //inicializa eloquent

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();
//el array que trae el controller, action y controller para middleware tiene que ser numerico
//por lo tanto se quitara las llaves 'controller', 'action', 'login', esto se entendera que quedara [0],[1],[2]
$map->get('index', '/', [
    'App\Controllers\IndexController',
     'indexAction',
    
]);

$map->get('addProyecto', '/proyecto/add', [
    'App\Controllers\ProyectoController',
     'getaddProyecto',
     true
]);
$map->post('saveProyecto', '/proyecto/add', [
    'App\Controllers\ProyectoController',
     'getaddProyecto',
     true
]);

$map->get('editProyects', '/proyecto/{id}/editar', [
    'App\Controllers\ProyectoController',
     'editAction'
]);
$map->post('editSaveProyects', '/proyecto/{id}/editar', [
    'App\Controllers\ProyectoController',
     'updateAction'
]);

$map->get('deletProyects', '/proyecto/{idp}/delete', [
    'App\Controllers\ProyectoController',
     'deletAction'
]);

$map->get('indexCostos', '/proyecto/{id}/costos/add', [
    'App\Controllers\IndexCostosController',
     'indexAction',
     true
]);

$map->get('addCostos', '/proyecto/{id}/costos/add/materiales', [
    'App\Controllers\CostosController',
     'getAddCostos',
     true
]);

$map->post('saveCostos', '/proyecto/{id}/costos/add/materiales', [
    'App\Controllers\CostosController',
     'getAddCostos',
     true
]);

$map->get('deletCostos', '/proyecto/{idp}/costos/{idc}/delete', [
    'App\Controllers\CostosController',
     'deletAction'
]);

$map->get('obrerosCostos', '/proyecto/{id}/costos/add/obreros', [
    'App\Controllers\IndexObrerosController',
     'indexAction',
     true
]);

$map->get('aparadoresCostos', '/proyecto/{id}/costos/add/obreros/aparadores', [
    'App\Controllers\AparadoresCostosController',
     'indexAction',
     true
]);

$map->get('armadoresCostos', '/proyecto/{id}/costos/add/obreros/armadores', [
    'App\Controllers\ArmadoresCostosController',
     'indexAction',
     true
]);

$map->get('addObrero', '/obrero/add', [
    'App\Controllers\ObreroController',
     'getObreros',
     true
]);
$map->post('saveObrero', '/obrero/add', [
    'App\Controllers\ObreroController',
     'getObreros',
     true
]);
$map->get('loginUser', '/login/user', [
    'App\Controllers\LoginUserController',
     'getLoginUser'
]);

$map->get('logoutUser', '/login/user', [
    'App\Controllers\LoginUserController',
     'getLogoutUser'
]);

$map->post('accessUser', '/login/user', [
    'App\Controllers\LoginUserController',
     'postLoginUser'
]);

$map->get('registerUser', '/register/user', [
    'App\Controllers\UsersController',
     'getAddUser'
]);
$map->post('addUser', '/register/user', [
    'App\Controllers\UsersController',
     'getAddUser'
]);
//---- API REST

$map->get('api.proyectos.get', '/api/v1/proyectos', [
    'App\Controllers\ProyectoController',
     'getApiProyect'
]);


//obtenemos el matcher que compara el objeto request con nuestro mapa
$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);
//var_dump($route); 
if(!$route){
    $emiter = new SapiEmitter();
     $emiter->emit( new Response\EmptyResponse(404));
}else{
    //Middleware : capas que se usan como filtros para validaciones, autenticaciones, etc.
    //de esta manera el cliente no tiene porque llegar hasta el cron de nuestra app
        //harmony dependencia que sirve para el uso de middlware que trabaja sobre zend/diactoros y para rutear usa fastroute, pero como son dependencias
        //que trabajan sobre psr entonces no deberia tener problemas de compatibilidad en este caso con
        //aurarouter q es la dependencia q usamos para rutear y trabaja sobre PSR-7, para hacer posible esto usamos una libreria middlewares/aura-router q implementa
        //aurarouter sobre PSR-15 y listo, este libreria mete a aurarouter en un contenedor que es compatible con los middleware
        //ahora es compatible

        //encerramos a harmony en un try catch para eviar un excepcion en caso algo falle 
        try{

            $harmony = new Harmony($request, new Response());
            $harmony
            ->addMiddleware(new HttpHandlerRunnerMiddleware(new SapiEmitter())) //esto emite una respuesta final
                ->addMiddleware(new \Franzl\Middleware\Whoops\WhoopsMiddleware)
            ->addMiddleware(new \App\Middlewares\AuthenticationMiddleware()) // aqui agregamos nuestro middleware de autenticacion de usuario que creeamos
            //este middleware autenticara al usuario y los datos no son correctos se le enviara una respuesta desde una primera capa
            ->addMiddleware(new \App\Middlewares\ValidatorCamposMiddleware()) 
            ->addMiddleware(new AuraRouter($routerContainer)) // aqui recibimos al contenedor de nuestras rutas creadas
            ->addMiddleware(new DispatcherMiddleware($container,'request-handler')); // Aqui recibimos un contenedor de inyeccion de dependencias y 
                                                                                      //lo que nos traer nuestro handler, que serian controller y action
        
            $harmony();
        // }catch(Exception $e){
        //     $log->warning($e->getMessage()); //creamos el error y con warning y el get->Message para capturar
        //                                            // el mensaje de la exception
        //     $emiter = new SapiEmitter(); // sapiemitter solo se encarga dee emitir algo en este caso excepciones
        //     $emiter->emit( new Response\EmptyResponse(404)); //emit funcion para emitir 
        
        // en php no podiamos cachear los errores ahora en php 7 si porque tanto excepcion como error 
        // son implementados en una interface de php llamada throwable
        }catch(Error $e){
            $emiter = new SapiEmitter();
            $emiter->emit( new Response\EmptyResponse(500));
        }
    //TODO ESTE CODIGO QUE SERVIA PARA ENVIAR UN REQUEST Y RESCIVIR UN RESPONSE HE INYECTAR DEPENDENCIAS NO SERAN NECESARIOS
    // AL USAR EL PAQUETE DE HARMONY ESE CODIGO HACE LO MISMO Q ESTA AQUI
    //----------------------------------------------------------------
    // $handlerData = $route->handler; //me trae el los parametros de cada ruta controller,acticion, login
    // //var_dump($handlerData);
    // $needsLogin = $handlerData['login'] ?? false;
    // $sessionUserId = $_SESSION['userid'] ?? null;
    // if($needsLogin && !$sessionUserId ){
    //     $controllerName = 'App\Controllers\LoginUserController';
    //     $actionName = 'getLogoutUser';
        
    // }else{
    //     $controllerName = $handlerData['controller'];
    //         $actionName = $handlerData['action'];
    // }
//    ----------------------------------------------------

    //TODO ESTE CODIGO QUE SERVIA PARA ENVIAR UN REQUEST Y RESCIVIR UN RESPONSE HE INYECTAR DEPENDENCIAS NO SERAN NECESARIOS
    // AL USAR EL PAQUETE DE HARMONY ESE CODIGO HACE LO MISMO Q ESTA AQUI
    //----------------------------------------------------------------
    //     $controller = $container->get($controllerName); //inyectamos la dependencia con get 
    //     //ejemplo si controlername trae a costoscontroller  este va a necesitar a costosservices por lo tanto
    //     //DI\Container() hace su funcion inyectando la dependencia q necesita costoscontroller
    //     $response = $controller->$actionName($request);
    //     //este foreach imprime cada headers que viene de como respuesta los headers guardan los estados de cada
    //     //pagina, por lo tanto imprime cada pagina q venga a travez de su header
    // foreach($response->getHeaders() as $name => $values){
    //     foreach($values as $value){
    //         header(sprintf('%s: %s',$name, $value),false);
    //     }
    // }
    // http_response_code($response->getStatusCode()); //statuscode si estado: 200 imprime la pagina porq existe
    // echo $response->getBody(); //imprimos el body
    //------------------------------------------------
}