<?php

namespace App\Services; 
 // implementamos un servicio para la inversion de dependencias (inyeccion de dependencias)
use App\Models\Proyectos;
use App\Models\Costos;

class DeleteServices {
// en este caso implementamos un servicio para eliminar data
    public function deleteServices($idp = null,$idc = null,$path){

        
           //si en caso el usuario quiere eliminar un dato que no se encuentra saldra un error de php 
        //entonces usamos el \Expception para enviar una excepcion
        if($path == "/proyecto/$idp/delete"){
            //var_dump($path);die;
            $proyectos = Proyectos::findOrFail($idp); // funcion de laravel eloquent que si no encuentra el objeto nos manda una excepcion
                                              // dependiendo de la excepcion
            $costos = Costos::where('idproyecto', '=',$idp)->delete();
             $proyectos->delete(); //metodo de eloquent laravel
        }

        if($path == "/proyecto/$idp/costos/$idc/delete"){
            //var_dump($path);die;
              //si en caso el usuario quiere eliminar un dato que no se encuentra saldra un error de php 
        //entonces usamos el \Expception para enviar una excepcion
        $costos = Costos::findOrFail($idc); // funcion de laravel eloquent que si no encuentra el objeto nos manda una excepcion
                                          // dependiendo de la excepcion
        $costos->delete(); //metodo de eloquent laravel
        }
    }   
     
}