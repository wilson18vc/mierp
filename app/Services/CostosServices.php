<?php

namespace App\Services; 
 // implementamos un servicio para la inversion de dependencias (inyeccion de dependencias)
use App\Models\Costos;

class CostosServices {
// en este caso implementamos un servicio para eliminar data
    public function deleteCostos($id){
           //si en caso el usuario quiere eliminar un dato que no se encuentra saldra un error de php 
        //entonces usamos el \Expception para enviar una excepcion
        $costos = Costos::findOrFail($id); // funcion de laravel eloquent que si no encuentra el objeto nos manda una excepcion
                                          // dependiendo de la excepcion
        $costos->delete(); //metodo de eloquent laravel
        
    }   

}