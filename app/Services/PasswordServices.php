<?php

namespace App\Services; 
 // implementamos un servicio para la inversion de dependencias (inyeccion de dependencias)
use App\Models\Costos;

class PasswordServices {
// en este caso implementamos un servicio para eliminar data
        public function generatePass($length  = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}