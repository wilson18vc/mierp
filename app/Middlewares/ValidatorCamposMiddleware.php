<?php
//creo una clase de auntenticacion usando Middleware  para autenticar la sesion del usuario
namespace App\Middlewares;

//estos namespace se instancian para poder usar la interface de middlewareinterface el request y el handler y un estado de respuesta
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\EmptyResponse;

use App\Models\Costos;
use App\Models\Proyectos;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use Zend\Diactoros\ServerRequest;
use Zend\Diactoros\Response\RedirectResponse;
use App\Controllers\BaseController;
use App\Controllers\BaseController\renderHtml;
use App\Controllers\CostosController;


//usamos harmony q es un dist patcher compatible con psr15
class ValidatorCamposMiddleware  extends BaseController implements MiddlewareInterface 
{

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
         $responseMessage = '';
        $imgValidator = '';
        
if ($request->getUri()->getPath() === '/proyecto/{id}/costos/add') {
        if($request->getMethod() == 'POST') {

            $postData = $request->getParsedBody();

            $files = $request->getUploadedFiles();
            $imgfile = $files['imgfile'];
           
            $costosValidator = v::key('nombre', v::stringType()->notEmpty())
            ->key('descripcion', v::stringType()->notEmpty())
            ->key('cantidad', v::floatVal()->notEmpty())
            ->key('preuni', v::floatVal()->notEmpty());
                
            if($imgfile->getError() == UPLOAD_ERR_OK){
                    $imgName = $imgfile->getClientFilename();
                     $ext = explode('.',$imgName);
                    if($ext[1] != 'jpg' && $ext[1] != 'png' && $ext[1] != 'jpeg' ){
                     $responseMessage = (['imagen' => 'Imagen : use formatos jpg, png o jpeg']);

                        $costs = Costos::where('idusuario', '=',$_SESSION['userid'])->get(); 
                        return $this->renderHtml('addCostos.twig',[
                            'costs' => $costs,
                            'responseMessage' => $responseMessage
                        ]);
                    }                 
                    }

            try {
                
                $costosValidator->assert($postData);
            } catch(NestedValidationException $exception) {
                $responseMessage = $exception->findMessages([
                    'cantidad' => 'Cantidad: Ingrese números',
                    'preuni' => 'Precio: Ingrese números'
                    ]);
                $costs = Costos::where('idusuario', '=',$_SESSION['userid'])->get(); 
                 return $this->renderHtml('CostosProyecto/addCostos.twig',[
                'costs' => $costs,
                'responseMessage' => $responseMessage
            ]);
            }
                
            
            }
}

if ($request->getUri()->getPath() === '/proyecto/add') {
        if($request->getMethod() == 'POST') {

            $postData = $request->getParsedBody();

            $files = $request->getUploadedFiles();
            $imgfile = $files['imgfile'];
           
            $costosValidator = v::key('nombre', v::stringType()->notEmpty())
            ->key('descripcion', v::stringType()->notEmpty())
            ->key('cantidad', v::floatVal()->notEmpty());
                
            if($imgfile->getError() == UPLOAD_ERR_OK){
                    $imgName = $imgfile->getClientFilename();
                     $ext = explode('.',$imgName);
                    if($ext[1] != 'jpg' && $ext[1] != 'png' && $ext[1] != 'jpeg' ){
                     $responseMessage = (['imagen' => 'Imagen : use formatos jpg, png o jpeg']);

                        $proyects = Proyectos::where('idusuario', '=',$_SESSION['userid'])->get(); 
                        return $this->renderHtml('Proyectos/addProyecto.twig',[
                            'proyects' => $proyects,
                            'responseMessage' => $responseMessage
                        ]);
                    }                 
                    }

            try {
                
                $costosValidator->assert($postData);
            } catch(NestedValidationException $exception) {
                $responseMessage = $exception->findMessages([
                    'cantidad' => 'Cantidad: Ingrese números',
                    ]);
                $proyects = Proyectos::where('idusuario', '=',$_SESSION['userid'])->get(); 
                 return $this->renderHtml('Proyectos/addProyecto.twig',[
                'proyects' => $proyects,
                'responseMessage' => $responseMessage
            ]);
            }
                
            
            }
}

if ($request->getUri()->getPath() === '/obrero/add') {
        if($request->getMethod() == 'POST') {

            $postData = $request->getParsedBody();

            $files = $request->getUploadedFiles();
            $imgfile = $files['imgfile'];
           
            $obreroValidator = v::key('nombres', v::stringType()->notEmpty())
            ->key('apellidos', v::stringType()->notEmpty())
            ->key('dni', v::stringType()->length(8, 8)->notEmpty())
            ->key('celular', v::stringType()->length(6, 9)->notEmpty());

                
            if($imgfile->getError() == UPLOAD_ERR_OK){
                    $imgName = $imgfile->getClientFilename();
                     $ext = explode('.',$imgName);
                    if($ext[1] != 'jpg' && $ext[1] != 'png' && $ext[1] != 'jpeg' ){
                     $responseMessage = (['imagen' => 'Imagen : use formatos jpg, png o jpeg']);

                        $proyects = Proyectos::where('idusuario', '=',$_SESSION['userid'])->get(); 
                        return $this->renderHtml('Proyectos/addProyecto.twig',[
                            'proyects' => $proyects,
                            'responseMessage' => $responseMessage
                        ]);
                    }                 
                    }

            try {
                
                $obreroValidator->assert($postData);
            } catch(NestedValidationException $exception) {
                $responseMessage = $exception->findMessages([
                    'dni' => 'DNI: Ingrese un dni valido',
                    'celular' => 'Celular: Ingrese un celular/telefono valido'
                    ]);
                //$proyects = Proyectos::where('idusuario', '=',$_SESSION['userid'])->get(); 
                 return $this->renderHtml('Proyectos/addProyecto.twig',[
                //'proyects' => $proyects,
                'responseMessage' => $responseMessage
            ]);
            }
                
            
            }
}
        
             return $handler->handle($request);             

    }
        
}