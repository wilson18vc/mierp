<?php
//creo una clase de auntenticacion usando Middleware  para autenticar la sesion del usuario
namespace App\Middlewares;

//estos namespace se instancian para poder usar la interface de middlewareinterface el request y el handler y un estado de respuesta
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\RedirectResponse;
//implementamos psr-15 server request handler q son la parte de como responder a un request
class AuthenticationMiddleware implements MiddlewareInterface
{

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        //getUri->getPath()  : obtenemos la url de la app
        if ($request->getUri()->getPath() === '/proyecto/add') {
            $sessionUserId = $_SESSION['userid'] ?? null;
            if (!$sessionUserId) {
                return new RedirectResponse('/login/user');
            }

        }
        
        return $handler->handle($request);
    }
}