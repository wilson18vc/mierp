<?php 

namespace App\Controllers;
use Zend\Diactoros\ServerRequest;


class ArmadoresCostosController extends BaseController {

       public function indexAction(ServerRequest $request){
        $params = $request->getAttributes();
         $proyecto = $params['id'];
        
        return $this->renderHtml('CostosProyecto/addCostoArmador.twig',[
            'proyecto' => $proyecto
        ]);
        }

}