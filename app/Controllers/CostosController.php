<?php

namespace App\Controllers;
use App\Models\Costos;
use App\Models\Proyectos;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use Zend\Diactoros\ServerRequest;
use Zend\Diactoros\Response\RedirectResponse;
use App\Services\DeleteServices;


class CostosController extends BaseController{

    private $requestServices; //implementando inversion de dependencias (inyectndo dependencias)

    public function __construct(DeleteServices $requestServices){
        parent::__construct();
        $this->requestServices = $requestServices;
    }
    public function getAddCostos(ServerRequest $request){
        $paramid = $request->getAttributes();
        $id = $paramid['id'];
        $responseMessage = '';
        $imgValidator = '';
         if($request->getMethod() == 'POST') {
                    
                $files = $request->getUploadedFiles();
                $imgfile = $files['imgfile'];

                $postData = $request->getParsedBody();
                $costos = new Costos();
                $costos->nombre = $postData['nombre'];
                $costos->idproyecto = $id;
                $costos->descripcion = $postData['descripcion'];
                $costos->cantidad = $postData['cantidad'];
                $costos->preciounidad = $postData['preuni'];
                $costos->preciototal = ($postData['cantidad'] * $postData['preuni'] ) ;
                $costos->idusuario = $_SESSION['userid'];
                if($imgfile->getError() == UPLOAD_ERR_OK){
                $imgfile = $files['imgfile'];
                $imgName = $imgfile->getClientFilename();
                $ext = explode('.',$imgName);
                $imgNewName = $ext[0]. date('Y-m-d') . "_" . date('H-i-s').'.'.$ext[1];
                $imgfile->moveTo("uploads/$imgNewName");
                $costos->imagen = "uploads/$imgNewName";
                }
                $responseMessage = ['Mensaje' => 'Se guardo exitosamente'] ;
                $costos->save();
                }
            $proyects = Proyectos::where('id','=',$id )->get();
            
            $costs = Costos::where('idusuario', '=',$_SESSION['userid'] )
            ->where('idproyecto', '=',$id )
            ->get();
            return $this->renderHtml('CostosProyecto/addCostos.twig',[
                'proyects' => $proyects,
                'costs' => $costs,
                'responseMessage' => $responseMessage
            ]);
                           
          
    }
    public function deletAction(ServerRequest $request){
        //var_dump($request);die;
        $params = $request->getAttributes();
        $path = $request->getUri()->getPath();
        $this->requestServices->deleteServices($params['idp'],$params['idc'],$path);
        return new RedirectResponse('/proyecto/'.$params['idp'].'/costos/add/materiales');
    }

}