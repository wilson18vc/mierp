<?php
namespace App\Controllers;
use App\Models\Users;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use App\Services\PasswordServices;



class UsersController extends BaseController{
    private $passServices;
     public function __construct(PasswordServices $passServices){
        parent::__construct();
        $this->passServices = $passServices;
    }
    public function getAddUser($request){
        $responseMessage = '';
        $imgValidator = '';
         if($request->getMethod() == 'POST') {
            $postData = $request->getParsedBody();
            $UserValidator = v::key('nombreuser', v::stringType()->notEmpty())
            ->key('correo', v::email()->notEmpty())
            ->key('apellidos', v::stringType()->notEmpty())
            ->key('password', v::stringType()->notEmpty());      

             try {
            $UserValidator->assert($postData);
            $users = new Users();
            $users->nombre = $postData['nombreuser'];
            $users->correo = $postData['correo'];
            $users->apellidos = $postData['apellidos'];
            $users->send = false;
            $pass = $this->passServices->generatePass();
            //$this->passServices->getPass($pass);
            $users->password = password_hash($postData['password'], PASSWORD_DEFAULT);
            $users->save();           
                        

            $responseMessage = 'Se guardo exitosamente';     
                              
             } catch(NestedValidationException $exception) {
             $responseMessage = $exception->getMessage();
            //     //$imgValidator = $exception->getMessage();
             }
            
            }
            //$costs = Costos::all(); 
            return $this->renderHtml('Auth/addUsers.twig',[
                'responseMessage' => $responseMessage,
            ]);
                           
          
    }

}