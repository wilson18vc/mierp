<?php 

namespace App\Controllers;
use Zend\Diactoros\ServerRequest;

class IndexCostosController extends BaseController {

    public function indexAction(ServerRequest $request){
        $params = $request->getAttributes();
         $proyecto = $params['id'];
        
        return $this->renderHtml('CostosProyecto/indexCostos.twig',[
            'proyecto' => $proyecto
        ]);
        }

}