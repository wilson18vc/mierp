<?php 

namespace App\Controllers;
use Zend\Diactoros\ServerRequest;


class AparadoresCostosController extends BaseController {

       public function indexAction(ServerRequest $request){
        $params = $request->getAttributes();
         $proyecto = $params['id'];
        
        return $this->renderHtml('CostosProyecto/addCostoAparador.twig',[
            'proyecto' => $proyecto
        ]);
        }

}