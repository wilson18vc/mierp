<?php

namespace App\Controllers;
use App\Models\Obreros;
use App\Models\Especialidades;
use Zend\Diactoros\ServerRequest;

class ObreroController extends BaseController
{
    public function __construct(){
        parent::__construct();
        
    }
    
public function getObreros(ServerRequest $request){

    $responseMessage = '';
    if($request->getMethod() == 'POST'){
         
        $files = $request->getUploadedFiles();
        $imgfile = $files['imgfile'];

        $params = $request->getParsedBody();
        $obrero = new Obreros;
        $obrero->nombres = $params['nombres'];
        $obrero->apellidos = $params['apellidos'];
        $obrero->dni = $params['dni'];
        $obrero->celular = $params['celular'];
        $obrero->id_especialidad = $params['especialidad'];
        if($imgfile->getError() == UPLOAD_ERR_OK){
                $imgfile = $files['imgfile'];
                $imgName = $imgfile->getClientFilename();
                $ext = explode('.',$imgName);
                $imgNewName = $ext[0]. date('Y-m-d') . "_" . date('H-i-s').'.'.$ext[1];
                $imgfile->moveTo("uploads/obreros/$imgNewName");
                $obrero->foto = "uploads/obreros/$imgNewName";
                }
                $responseMessage = ['Mensaje' => 'Se guardo exitosamente'];
        $obrero->save();
     }


    $especialidad =  Especialidades::all();
    $obreros =  Obreros::all();
 
    return $this->renderHtml('Obreros/addObreros.twig',[
        'especialidad' => $especialidad,
        'obreros' => $obreros,
        'responseMessage'=> $responseMessage
    ]);

}




}
