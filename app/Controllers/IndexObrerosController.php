<?php 

namespace App\Controllers;
use Zend\Diactoros\ServerRequest;

class IndexObrerosController extends BaseController {

    public function indexAction(ServerRequest $request){
        $params = $request->getAttributes();
         $proyecto = $params['id'];
        
        return $this->renderHtml('CostosProyecto/indexObreros.twig',[
            'proyecto' => $proyecto
        ]);
        }

}