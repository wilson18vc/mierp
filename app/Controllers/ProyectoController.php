<?php 
namespace App\Controllers;
use App\Models\Proyectos;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use Zend\Diactoros\ServerRequest;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Response\JsonResponse;
use App\Services\DeleteServices;


class ProyectoController extends BaseController {

    private $requestServices; //implementando inversion de dependencias (inyectndo dependencias)

    public function __construct(DeleteServices $requestServices){
        parent::__construct();
        $this->requestServices = $requestServices;
    }

    public function getaddProyecto(ServerRequest $request){
            $responseMessage = '';
            if($request->getMethod() == 'POST'){
                $files = $request->getUploadedFiles();//el request me trae el uploadfile donde este obtiene  la imagen subida
                $imgfile = $files['imgfile'];
                
                $postData = $request->getParsedBody();
                $proyectos = new Proyectos;
                $proyectos->nombre = $postData['nombre'];
                $proyectos->descripcion = $postData['descripcion'];
                $proyectos->capital = $postData['cantidad'];
                $proyectos->idusuario = $_SESSION['userid'];

                
                if($imgfile->getError() == UPLOAD_ERR_OK){ // el error es 0 si es 0 entonces esto se cumple
                $imgName = $imgfile->getClientFilename(); //clientFilename obtiene el nombre de la imagen
                $ext = explode('.',$imgName);
                $imgNewName = $ext[0]. date('Y-m-d') . "_" . date('H-i-s').'.'.$ext[1];
                $imgfile->moveTo("uploads/$imgNewName");
                $proyectos->imagen = "uploads/$imgNewName";
                }
                $responseMessage = 'Se guardo exitosamente';
                $proyectos->save();
            }
            $proyects = Proyectos::all(); 
            return $this->renderHtml('Proyectos/addProyecto.twig',[
                 'proyects' => $proyects,
                'responseMessage' => $responseMessage
            ]); 
    }
      public function deletAction(ServerRequest $request){
        $params = $request->getAttributes();
        $path = $request->getUri()->getPath();
        $this->requestServices->deleteServices($params['idp'],null,$path);
        return new RedirectResponse('/proyecto/add');
    }

     public function editAction(ServerRequest $request){

               
        $params = $request->getAttributes();
        $proyects = Proyectos::findOrFail($params['id']);
        return $this->renderHtml('Proyectos/editProyecto.twig',[
                 'proyects' => $proyects,
            ]); 
    }

    public function updateAction(ServerRequest $request){

               
        $params = $request->getAttributes();
        $proyects = Proyectos::findOrFail($params['id']);
        if($request->getMethod() == 'POST'){
            $files = $request->getUploadedFiles();//el request me trae el uploadfile donde este obtiene  la imagen subida
                $imgfile = $files['imgfile'];
                
                $postData = $request->getParsedBody();
                $proyectos = new Proyectos;
                $proyectos->nombre = $postData['nombre'];
                $proyectos->descripcion = $postData['descripcion'];
                $proyectos->capital = $postData['cantidad'];                
                if($imgfile->getError() == UPLOAD_ERR_OK){ // el error es 0 si es 0 entonces esto se cumple
                $imgName = $imgfile->getClientFilename(); //clientFilename obtiene el nombre de la imagen
                $ext = explode('.',$imgName);
                $imgNewName = $ext[0]. date('Y-m-d') . "_" . date('H-i-s').'.'.$ext[1];
                $imgfile->moveTo("uploads/$imgNewName");
                $proyectos->imagen = "uploads/$imgNewName";
                }
                $proyects::where('id', $params['id'])
                          ->update(['nombre' => $postData['nombre'],
                                    'descripcion' => $postData['descripcion'],
                                    'capital' => $postData['cantidad'] ]);
                           return new RedirectResponse('/proyecto/add'); 


               }
    }

    public function getApiProyect($request){

        $proyectos = Proyectos::all();
        return new JsonResponse($proyectos);
    }

}