<?php
namespace App\Controllers;
use App\Models\Users;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\ServerRequest;


class LoginUserController extends BaseController{

    public function getLoginUser(ServerRequest $request){

      return $this->renderHtml('Auth/login.twig');                         
          
    }
    public function postLoginUser($request){
      
        $responseMessage = '';
        $imgValidator = '';
        
         if($request->getMethod() == 'POST') {
            $UserValidator = v::key('email', v::email()->notEmpty())   
            ->key('password', v::stringType()->notEmpty());
            $postData = $request->getParsedBody();
            $user = Users::where('correo', $postData['email'])->first();          
            try {
                $UserValidator->assert($postData);
            if($user){
                if(\password_verify($postData['password'], $user->password)){
                    $_SESSION['userid'] = $user->id; 
                    return new RedirectResponse('/');
                }else{
                    $responseMessage = 'Correo o contraseña incorrecta';
                }

            }else{
                $responseMessage = 'Correo o contraseña incorrecta';
            }
                          
            }catch(NestedValidationException $exception) {
                $responseMessage = $exception->getMessage();
            }
        
        }
        return $this->renderHtml('Auth/login.twig',[
            'responseMessage' => $responseMessage,
        ]);
                       
      
}

        public function getLogoutUser($request){
            unset($_SESSION['userid']);
            return new RedirectResponse('/login/user');
        }


    

}