<?php

namespace App\Traits;

trait HasDefaultImage  // los traits son como una clase se puede instanciar usando la palabra "use"
                       // un trait implementa codigo que no necesariamente debe estar relacionado con una clase
                       //por ejemplo hacer uso de listado de imagenes puedo usar este trait en tantas clases como me se conveniente
{
  public function getImage(){
    if($this->imagen){
      return $this->imagen;
    }elseif($this->foto){
      return $this->foto;
      
    }else{
      return 'uploads/sin-foto-514.jpg';
    } 
  }  
}
