<?php

namespace App\Models;

class BaseElement{

    protected $nombreProducto;
    protected $cantidadUnidad;
    protected $precioUnidad;
    protected $cantidadTotal;
    protected $precioTotal;

    function setNombreProducto($nombreProducto){
        $this->nombreProducto = $nombreProducto;
    }
    function getNombreProducto(){
        return $this->nombreProducto;
    }

    function setCantidadUnidad($cantidadUnidad){
        $this->cantidadUnidad = $cantidadUnidad;
    }
    function getCantidadUnidad(){
        return $this->cantidadUnidad;
    }

    function setCantidadTotal($cantidadTotal){
        $this->cantidadTotal = $cantidadTotal ;
    }
    function getCantidadTotal(){
        return $this->cantidadTotal;
    }
    function setPrecioUnidad($precioUnidad){
         $this->precioUnidad = $precioUnidad;
    }

    function getPrecioUnidad(){
        return $this->precioUnidad;
    }

    function getPrecioTotal(){
        return $this->getPrecioUnidad() * $this->getCantidadTotal();
    }

}