<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasDefaultImage;

class Obreros extends Model
{
    use HasDefaultImage;

    protected $table = 'tbl_obreros';
    
}
