<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;// uso de la dependencia de eloquent para el mapeo de BD
use App\Traits\HasDefaultImage; // uso del trait HasDefaultImage para obtener la imagen de la bd (ruta)
                                // y si en caso no el trait implementa una ruta de imagen diferente
use Illuminate\Database\Eloquent\SoftDeletes;// uso de la dependencia de eloquent para la tecnica de ocultar
                                            // datos que aparentan ser eliminados pero en la BD no

class Proyectos extends Model{
    use HasDefaultImage;
    use SoftDeletes; //es un trait de eloquent para ocultar datos aparentando que se han borrado
                      // pero real mente siguen en la BD

    // public  function costos(){
    //     return $this->hasMany(Costos::class); //hasMany indica una relacion de uno a muchos (expensereport itene varios expense)
    // }

    protected $table = 'tbl_proyectos'; 

}